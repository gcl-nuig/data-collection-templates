# Data collection templates

A collection of file templates for data collection.  The templates are
grouped by method (SOP-XX-X) and available in various formats (e.g.,
org-mode, tab-separated-values, etc).  Files that do not belong to an
SOP are in the `unclassified` folder.
